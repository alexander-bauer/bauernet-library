#########
Changelog
#########

All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog <https://keepachangelog.com/en/1.0.0/>`_,
and this project adheres to `Semantic Versioning <https://semver.org/spec/v2.0.0.html>`_.

[Unreleased]
============

Added
-----

* README to ``template/new``

Changed
-------

* Fixed changelog formatting
* Disabled Spheode pre-commit hook for now

[0.2.0] - 2023-05-01
====================

Added
-----

* Template for ``docs/`` first-time setup
* ``.gitignore`` for this repository

Changed
-------

* Improved ``rsync`` flag reusage in ``sync.sh``

[0.1.0] - 2023-05-01
====================

Added
^^^^^

* Repository boilerplate
* Initial template and sync logic
