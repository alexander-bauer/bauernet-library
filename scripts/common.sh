#!/bin/bash
#
# common.sh
#
# This script determines commonly-needed values and exports them.

set -eu -o pipefail

SCRIPTS="$(dirname "$0")"
export SCRIPTS

LIBRARY_ROOT="$(readlink -f "$SCRIPTS/../")"
export LIBRARY_ROOT

REPO_ROOT="$(readlink -f "$LIBRARY_ROOT/../")"
export REPO_ROOT

REPO_LIST="$(find "$REPO_ROOT" -maxdepth 1 -type d -name "bauernet-*")"
export REPO_LIST
