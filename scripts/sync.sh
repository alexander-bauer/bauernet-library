#!/bin/bash
#
# sync.sh
#
# This script is used to synchronize the repository template into other
# BauerNet repositories.

source "$(dirname "$0")/common.sh"

# Create a function to wrap rsync with common flags.
rsync() { command rsync -rli --ignore-times --checksum "$@"; }

for repo in $REPO_LIST; do
  echo "Synchronizing $repo" 1>&2
  rsync --ignore-existing "$LIBRARY_ROOT/template/new/" "$repo"
  rsync "$LIBRARY_ROOT/template/always/" "$repo"
done
